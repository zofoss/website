if (localStorage.getItem("pref-theme") === "dark") {
  document.documentElement.classList.add("dark");
} else if (localStorage.getItem("pref-theme") === "light") {
  document.documentElement.classList.remove("dark");
} else if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
  document.documentElement.classList.add("dark");
}
