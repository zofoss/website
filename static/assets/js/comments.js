function loadComments() {
  const script = document.createElement("script");
  script.setAttribute("src", "https://giscus.app/client.js");
  script.setAttribute("class", "giscus_comments");
  script.setAttribute("data-repo", "zofoss/website-comments");
  script.setAttribute("data-repo-id", "R_kgDOHOCf3A");
  script.setAttribute("data-category", "General");
  script.setAttribute("data-category-id", "DIC_kwDOHOCf3M4COu8s");
  script.setAttribute("data-mapping", "title");
  script.setAttribute("data-reactions-enabled", "1");
  script.setAttribute("data-emit-metadata", "0");
  script.setAttribute("data-input-position", "top");
  script.setAttribute(
    "data-theme",
    localStorage.getItem("pref-theme")
      ? localStorage.getItem("pref-theme")
      : window.matchMedia("(prefers-color-scheme: dark)").matches
      ? "dark"
      : "light"
  );
  script.setAttribute("data-lang", "en");
  script.setAttribute("data-loading", "lazy");
  script.setAttribute("crossorigin", "anonymous");
  script.setAttribute("async", "");
  script.onload = function handleScriptLoaded() {
    document.querySelector("#theme-toggle").addEventListener("click", () => {
      let e = document.querySelector("iframe.giscus-frame");
      e &&
        e.contentWindow.postMessage(
          {
            giscus: {
              setConfig: {
                theme: localStorage.getItem("pref-theme")
                  ? localStorage.getItem("pref-theme")
                  : window.matchMedia("(prefers-color-scheme: dark)").matches
                  ? "dark"
                  : "light",
              },
            },
          },
          "https://giscus.app"
        );
    });
  };

  script.onerror = function handleScriptError() {
    console.log("error loading comments!");
  };

  document.querySelector(".cm-giscus").appendChild(script);
}
