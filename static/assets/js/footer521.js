document.querySelectorAll('a[href^="#"]').forEach((anchor) => {
  anchor.addEventListener("click", function (e) {
    e.preventDefault();
    var id = this.getAttribute("href").substr(1);
    if (!window.matchMedia("(prefers-reduced-motion: reduce)").matches) {
      document
        .querySelector(`[id='${decodeURIComponent(id)}']`)
        .scrollIntoView({
          behavior: "smooth",
        });
    } else {
      document
        .querySelector(`[id='${decodeURIComponent(id)}']`)
        .scrollIntoView();
    }
    if (id === "top") {
      history.replaceState(null, null, " ");
    } else {
      history.pushState(null, null, `#${id}`);
    }
  });
});
function openMenu(x) {
  x.classList.toggle("me-new");
  document.querySelector(".flexdis").classList.toggle("op-menu");
  document.querySelector("html").classList.toggle("noscroll");
}
if (navigator.userAgent.search("Firefox") > -1) {
  document
    .querySelector(".logo a")
    .setAttribute("title", "ZoFoss (Alt + Shift + H)");
  document
    .querySelector(".top-link")
    .setAttribute("title", "Go to Top (Alt + Shift + G)");
  document
    .querySelector("button#theme-toggle")
    .setAttribute("title", "Toggle Theme (Alt + Shift + T)");
  document
    .querySelector("#search-btn")
    .setAttribute("title", "Search (Alt + Shift + /)");
}
var mybutton = document.getElementById("top-link");
window.onscroll = function () {
  backtoTop();
};
document.getElementById("theme-toggle").addEventListener("click", () => {
  if (document.documentElement.className.includes("dark")) {
    document.documentElement.classList.remove("dark");
    localStorage.setItem("pref-theme", "light");
  } else {
    document.documentElement.classList.add("dark");
    localStorage.setItem("pref-theme", "dark");
  }
});
// Settings Back to Top hide
if (localStorage.getItem("hide-back") === "true") {
  document.getElementById("top-link").style.display = "none";
}
// Hide Title bar on Scroll
if (localStorage.getItem("hide-title") === "true") {
document.querySelector("html").classList.add("hidetitlebar");
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.querySelector(".nav").style.top = "0";
  } else {
    document.querySelector(".nav").style.top = "-61px";
  }
  prevScrollpos = currentScrollPos;
  backtoTop();
}
}

function backtoTop() {
  if (document.body.scrollTop > 313 ||
    document.documentElement.scrollTop > 313) {
    mybutton.style.visibility = "visible";
    mybutton.style.opacity = "1";
    mybutton.blur();
  } else {
    mybutton.style.visibility = "hidden";
    mybutton.style.opacity = "0";
  }
}
