const paramsString = location.search;
let searchParams = new URLSearchParams(paramsString);
const searchQuery = searchParams.get("q");
const searchInput = document.querySelector("input#searchInput");
searchInput.value = searchQuery;
