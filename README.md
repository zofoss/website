# ZoFoss Website
A public platform for the tech/foss community built by the community.


## Dependencies
- Hugo
- PaperMod Theme


## Deployment
To deploy this project run

```bash
  hugo
```


## Acknowledgements
 - [Hugo Framework](https://gohugo.io/)
 - [PaperMod Theme](https://github.com/adityatelange/hugo-PaperMod/)
 - [More on Website](https://www.zofoss.org/credits)


## License
- Code of this website is licensed under the MIT License. See [LICENSE](https://gitlab.com/zofoss/website/-/blob/main/LICENSE) for details.
- Content of this website is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) License.


