---
title: "Fix: Fedora 37 (GNOME 43) Device Security Page Missing"
date: 2022-09-17
tags: ["linux", "GNOME", "Fedora", "fix"]
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Is GNOME 43 Device Security page missing from privacy settings? Don't worry! We have got you covered."
disableShare: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowPostNavLinks: true
cover:
    image: "assets/img/blog/device-security-missing-fedora37.png" # image path/url
    alt: "Fix: Fedora 37 (GNOME 43) Device Security Page Missing" # alt text
    caption: "Fix: Fedora 37 (GNOME 43) Device Security Page Missing" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
Is your Device Security page missing from privacy settings? Don't worry! We have got you covered.  
This is happening because of a recent [commit](https://gitlab.gnome.org/GNOME/gnome-control-center/-/commit/0245329949957e7f7ab56f240faaf336408ced86) made by GNOME devs which resulted in the device security section getting hidden from privacy settings, for virtual machines and some other devices too.

## Why it's happening? How to fix it?
Most probably because you are in a virtual machine and this feature isn't intended for virtual machines another reason may be due to some device-related issue that shows your `chassis` type is empty.  
There's an easy fix just need to set chassis type:
- Desktop Users: `hostnamectl set-chassis desktop`
- Laptop Users: `hostnamectl set-chassis laptop`

## More Details from `man` command
```
NAME
       hostnamectl - Control the system hostname

SYNOPSIS
       hostnamectl [OPTIONS...] {COMMAND}

------
------
COMMANDS
       The following commands are understood:

       status
           Show system hostname and related information. If no command is
           specified, this is the implied default.

------
------

       chassis [TYPE]
           If no argument is given, print the chassis type. If an optional
           argument TYPE is provided then the command changes the chassis type
           to TYPE. The chassis type is used by some graphical applications to
           visualize the host or alter user interaction. Currently, the
           following chassis types are defined: "desktop", "laptop",
           "convertible", "server", "tablet", "handset", "watch", "embedded",
           as well as the special chassis types "vm" and "container" for
           virtualized systems that lack an immediate physical chassis.
------
------
```