---
title: "Customizing Cursors in GNOME"
date: 2022-06-01
tags: ["linux", "GNOME", "Fedora", "Ubuntu", "customize"]
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "In this article, we will learn how to customize cursors in GNOME, change particular cursor in theme, and change the size of the cursor in GNOME."
disableShare: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowPostNavLinks: true
cover:
    image: "assets/img/blog/customizing-cursors-in-gnome.png" # image path/url
    alt: "Customizing Cursors in GNOME" # alt text
    caption: "Customizing Cursors in GNOME" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
In this article, we will learn how to customize cursors in GNOME, change the cursors **theme**, change **particular cursor** in the theme, and change the **size of the cursor** in GNOME.

## 🤔 Why Customize Cursors?
Customization in Linux is fun you can use different and unique cursor themes to give your cursor a cool look or you can change cursor size to make according to your preference because the cursor plays a very important role in working on our desktop.



## 📄 Pre Requirements
- Linux Distro Installed with GNOME Desktop Environment
- Basic Linux Knowledge



## Changing the Size of the Mouse Pointer
In GNOME desktop you don't generally get an option in settings to change cursor size but it's possible and it's very easy you just need to run one-line command.

So launch terminal and run the following command:
```sh
gsettings set org.gnome.desktop.interface cursor-size 30
```
Where "30" is the cursor size. The default size is 24, you can set a large number to increase and a smaller number to decrease the size.

In order to find your current cursor size you can run this command:
```sh
gsettings get org.gnome.desktop.interface cursor-size
```


## Changing Cursors Theme
So firstly you will have to find the theme you like the most, to find the theme go to [gnome-look.org](https://www.gnome-look.org/browse?cat=107) and choose any theme you like.

In our case we choose this one:
![Cursor Theme](/assets/img/blog/cursor-theme.png)
Now go to files tab and click on download button.
![Cursor Theme Download](/assets/img/blog/cursor-theme-download.png)

Now extract the archive file you downloaded, you will see a new folder with theme name in our case its name is "Plasma-Overdose" so now what you have to do is move this folder to `~/.icons` (.icons should be in the root of the home folder such that `/home/$USER/.icons`) please don't forget to enable **show hidden files** from 3-dot menu at the top in files app or you can also use the <kbd>Ctrl + H</kbd> shortcut to toggle hidden items.

> Note: Feel free to create `.icons` directory yourself if you don't have one.

To change your cursor theme you have **2 options** the first one is to **use terminal** and the second one is to use a **GUI tool** called GNOME Tweaks:
### 1. Using Terminal
To change the cursor theme launch the terminal and run the following command:
```sh
gsettings set org.gnome.desktop.interface cursor-theme Plasma-Overdose
```
Where "Plasma-Overdose" is the cursor theme. The default cursor theme is Adwaita, It may differ according to your Linux distro.

In order to find your current cursor theme you can run this command:
```sh
gsettings get org.gnome.desktop.interface cursor-theme
```
### 2. Using GNOME Tweaks (GUI)
To install GNOME Tweaks run the following command:

- Ubuntu: ```sudo apt install gnome-tweaks```
- Fedora: ```sudo dnf install gnome-tweaks```

Launch GNOME Tweaks and go to the appearance tab then in the cursor dropdown select your cursor theme.
![Select Cursor Theme](/assets/img/blog/select-cursor-theme.png)

🥳 Congratulations you have successfully installed and enabled your new cursor theme.


## Changing Particular Cursor in Cursor Theme
It's a very easy task you just have to play with files, replace them correctly and you are done.
So let's do this, We have taken [Adwaita](https://gitlab.gnome.org/GNOME/adwaita-icon-theme) and [Plasma-Overdose](https://www.gnome-look.org/p/1700441), For this example we have cloned the Adwaita theme into the `~/.icons` folder and renamed it to `AdwaitaClone` to avoid issues. We like the Adwaita theme but we want the text selection cursor from the Plasma-Overdose theme, So to do this refer to the below chart as you can see the text selection cursor file name is `text` so now what you have to do is replace `~/.icons/AdwaitaClone/cursors/text` with `~/.icons/Plasma-Overdose/cursors/text` and that's it you are done. Set the theme to AdwaitaClone and enjoy your custom theme. 

### Cursor Filename Chart
Opening every cursor file and seeing "Which cursor is this" is kinda annoying right?  
Don't worry! We have got you covered with the previews and file names of all the cursors.

| Preview | File Name |
|---|---|
| ![](/assets/img/blog/adwaita-cursors/alias.png) | alias |
| ![](/assets/img/blog/adwaita-cursors/all-scroll.png) | all-scroll |
| ![](/assets/img/blog/adwaita-cursors/left_ptr.png) | arrow |
| ![](/assets/img/blog/adwaita-cursors/bd_double_arrow.png) | bd_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/bottom_left_corner.png) | bottom_left_corner |
| ![](/assets/img/blog/adwaita-cursors/bottom_right_corner.png) | bottom_right_corner |
| ![](/assets/img/blog/adwaita-cursors/bottom_side.png) | bottom_side |
| ![](/assets/img/blog/adwaita-cursors/bottom_tee.png) | bottom_tee |
| ![](/assets/img/blog/adwaita-cursors/cell.png) | cell |
| ![](/assets/img/blog/adwaita-cursors/circle.png) | circle |
| ![](/assets/img/blog/adwaita-cursors/sb_h_double_arrow.png) | col-resize |
| ![](/assets/img/blog/adwaita-cursors/context-menu.png) | context-menu |
| ![](/assets/img/blog/adwaita-cursors/copy.png) | copy |
| ![](/assets/img/blog/adwaita-cursors/cross.png) | cross |
| ![](/assets/img/blog/adwaita-cursors/crossed_circle.png) | crossed_circle |
| ![](/assets/img/blog/adwaita-cursors/crosshair.png) | crosshair |
| ![](/assets/img/blog/adwaita-cursors/cross.png) | cross_reverse |
| ![](/assets/img/blog/adwaita-cursors/left_ptr.png) | default |
| ![](/assets/img/blog/adwaita-cursors/cross.png) | diamond_cross |
| ![](/assets/img/blog/adwaita-cursors/dnd-ask.png) | dnd-ask |
| ![](/assets/img/blog/adwaita-cursors/dnd-copy.png) | dnd-copy |
| ![](/assets/img/blog/adwaita-cursors/dnd-link.png) | dnd-link |
| ![](/assets/img/blog/adwaita-cursors/dnd-move.png) | dnd-move |
| ![](/assets/img/blog/adwaita-cursors/dnd-no-drop.png) | dnd-no-drop |
| ![](/assets/img/blog/adwaita-cursors/dnd-none.png) | dnd-none |
| ![](/assets/img/blog/adwaita-cursors/dotbox.png) | dotbox |
| ![](/assets/img/blog/adwaita-cursors/dotbox.png) | dot_box_mask |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | double_arrow |
| ![](/assets/img/blog/adwaita-cursors/right_ptr.png) | draft_large |
| ![](/assets/img/blog/adwaita-cursors/right_ptr.png) | draft_small |
| ![](/assets/img/blog/adwaita-cursors/dotbox.png) | draped_box |
| ![](/assets/img/blog/adwaita-cursors/right_side.png) | e-resize |
| ![](/assets/img/blog/adwaita-cursors/sb_h_double_arrow.png) | ew-resize |
| ![](/assets/img/blog/adwaita-cursors/fd_double_arrow.png) | fd_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/move.png) | fleur |
| ![](/assets/img/blog/adwaita-cursors/hand1.png) | grab |
| ![](/assets/img/blog/adwaita-cursors/grabbing.png) | grabbing |
| ![](/assets/img/blog/adwaita-cursors/hand2.png) | hand |
| ![](/assets/img/blog/adwaita-cursors/hand1.png) | hand1 |
| ![](/assets/img/blog/adwaita-cursors/hand2.png) | hand2 |
| ![](/assets/img/blog/adwaita-cursors/sb_h_double_arrow.png) | h_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/question_arrow.png) | help |
| ![](/assets/img/blog/adwaita-cursors/dotbox.png) | icon |
| ![](/assets/img/blog/adwaita-cursors/left_ptr.png) | left_ptr |
| ![](/assets/img/blog/adwaita-cursors/question_arrow.png) | left_ptr_help |
| ![](/assets/img/blog/adwaita-cursors/left_ptr_watch_0001.png) | left_ptr_watch |
| ![](/assets/img/blog/adwaita-cursors/left_side.png) | left_side |
| ![](/assets/img/blog/adwaita-cursors/left_tee.png) | left_tee |
| ![](/assets/img/blog/adwaita-cursors/link.png) | link |
| ![](/assets/img/blog/adwaita-cursors/ll_angle.png) | ll_angle |
| ![](/assets/img/blog/adwaita-cursors/lr_angle.png) | lr_angle |
| ![](/assets/img/blog/adwaita-cursors/move.png) | move |
| ![](/assets/img/blog/adwaita-cursors/top_right_corner.png) | ne-resize |
| ![](/assets/img/blog/adwaita-cursors/fd_double_arrow.png) | nesw-resize |
| ![](/assets/img/blog/adwaita-cursors/dnd-no-drop.png) | no-drop |
| ![](/assets/img/blog/adwaita-cursors/crossed_circle.png) | not-allowed |
| ![](/assets/img/blog/adwaita-cursors/top_side.png) | n-resize |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | ns-resize |
| ![](/assets/img/blog/adwaita-cursors/top_left_corner.png) | nw-resize |
| ![](/assets/img/blog/adwaita-cursors/bd_double_arrow.png) | nwse-resize |
| ![](/assets/img/blog/adwaita-cursors/hand1.png) | openhand |
| ![](/assets/img/blog/adwaita-cursors/pencil.png) | pencil |
| ![](/assets/img/blog/adwaita-cursors/X_cursor.png) | pirate |
| ![](/assets/img/blog/adwaita-cursors/plus.png) | plus |
| ![](/assets/img/blog/adwaita-cursors/hand2.png) | pointer |
| ![](/assets/img/blog/adwaita-cursors/pointer-move.png) | pointer-move |
| ![](/assets/img/blog/adwaita-cursors/left_ptr_watch_0001.png) | progress |
| ![](/assets/img/blog/adwaita-cursors/question_arrow.png) | question_arrow |
| ![](/assets/img/blog/adwaita-cursors/right_ptr.png) | right_ptr |
| ![](/assets/img/blog/adwaita-cursors/right_side.png) | right_side |
| ![](/assets/img/blog/adwaita-cursors/right_tee.png) | right_tee |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | row-resize |
| ![](/assets/img/blog/adwaita-cursors/sb_down_arrow.png) | sb_down_arrow |
| ![](/assets/img/blog/adwaita-cursors/sb_h_double_arrow.png) | sb_h_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/sb_left_arrow.png) | sb_left_arrow |
| ![](/assets/img/blog/adwaita-cursors/sb_right_arrow.png) | sb_right_arrow |
| ![](/assets/img/blog/adwaita-cursors/sb_up_arrow.png) | sb_up_arrow |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | sb_v_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/bottom_right_corner.png) | se-resize |
| ![](/assets/img/blog/adwaita-cursors/move.png) | size_all |
| ![](/assets/img/blog/adwaita-cursors/fd_double_arrow.png) | size_bdiag |
| ![](/assets/img/blog/adwaita-cursors/bd_double_arrow.png) | size_fdiag |
| ![](/assets/img/blog/adwaita-cursors/sb_h_double_arrow.png) | size_hor |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | size_ver |
| ![](/assets/img/blog/adwaita-cursors/bottom_side.png) | s-resize |
| ![](/assets/img/blog/adwaita-cursors/bottom_left_corner.png) | sw-resize |
| ![](/assets/img/blog/adwaita-cursors/dotbox.png) | target |
| ![](/assets/img/blog/adwaita-cursors/tcross.png) | tcross |
| ![](/assets/img/blog/adwaita-cursors/xterm.png) | text |
| ![](/assets/img/blog/adwaita-cursors/left_ptr.png) | top_left_arrow |
| ![](/assets/img/blog/adwaita-cursors/top_left_corner.png) | top_left_corner |
| ![](/assets/img/blog/adwaita-cursors/top_right_corner.png) | top_right_corner |
| ![](/assets/img/blog/adwaita-cursors/top_side.png) | top_side |
| ![](/assets/img/blog/adwaita-cursors/top_tee.png) | top_tee |
| ![](/assets/img/blog/adwaita-cursors/ul_angle.png) | ul_angle |
| ![](/assets/img/blog/adwaita-cursors/ur_angle.png) | ur_angle |
| ![](/assets/img/blog/adwaita-cursors/sb_v_double_arrow.png) | v_double_arrow |
| ![](/assets/img/blog/adwaita-cursors/vertical-text.png) | vertical-text |
| ![](/assets/img/blog/adwaita-cursors/watch_0001.png) | wait |
| ![](/assets/img/blog/adwaita-cursors/watch_0001.png) | watch |
| ![](/assets/img/blog/adwaita-cursors/left_side.png) | w-resize |
| ![](/assets/img/blog/adwaita-cursors/X_cursor.png) | X_cursor |
| ![](/assets/img/blog/adwaita-cursors/xterm.png) | xterm |
| ![](/assets/img/blog/adwaita-cursors/zoom-in.png) | zoom-in |
| ![](/assets/img/blog/adwaita-cursors/zoom-out.png) | zoom-out |

Cursor Image Credits: [Adwaita Theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme) licensed under [CC BY SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

## Conclusion
From this article, you came to know about cursor customization in GNOME. Feel free to comment if you have any doubts or questions.