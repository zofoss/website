---
title: "Credits"
showToc: false
hidemeta: true
comments: false
description: "Without them we are nothing!"
disableHLJS: true
disableShare: true
ShowReadingTime: false
hideJsonSchema: true
---
Without them we are nothing!
We wouldn't have come this far without their support.
## W3Schools
- [Toggle Button](https://www.w3schools.com/howto/howto_css_switch.asp) in Settings
- Hide Title Bar on Scroll in Settings uses script from [W3Schools](https://www.w3schools.com/howto/howto_js_navbar_hide_scroll.asp)

## Hugo
- Static Site Generator
- This website is built using [Hugo](https://gohugo.io)

## GitLab
- Code Storage
- Backup Storage

## Cloudflare
- Cloudflare [Pages](https://pages.dev/) for hosting
- Domain Management and other services

## GitHub Projects
- kbd tag css from [auth0](https://github.com/auth0/kbd)
- Comments are powered by [giscus](https://github.com/apps/giscus)
- [Animate.css](https://github.com/animate-css/animate.css): FadeIn and FadeInUp animation css

## Aditya Telange
- giscus light/dark theme auto shift inspiration from his [website](https://adityatelange.in/)
- Hugo [PaperMod](https://github.com/adityatelange/hugo-PaperMod) Theme

## Stack Overflow
- New tab link icon [css](https://stackoverflow.com/questions/1899772)

## MDN Docs
- For Web Basics
- For Browser compatibility charts so that we can optimize our website
- Edit this page box insipiration

## Codepen
- [Material Loader](https://codepen.io/astro785/full/xYbPZK)