---
title: "Search" # in any language you want
layout: "search" # is necessary
# url: "/archive"
# description: "Description for Search"
summary: "search"
placeholder: "Search ↵"
hideJsonSchema: true
disableHLJS: true
disableShare: true
ShowReadingTime: false
---